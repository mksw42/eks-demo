module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "17.1.0"
  cluster_name = local.cluster_name
  cluster_version = "1.27"
  subnets = module.vpc.private_subnets
  tags = {
    Name = "EKS-Cluster-DEV-DIGMIA"
  }
  vpc_id = module.vpc.vpc_id
  workers_group_defaults = {
    root_volume_type = "gp2"
  }
  worker_groups = [
    {
      name = "First-Group"
      instance_type = "t3a.medium"
      asg_desired_capacity = 1
      autoscaling_enabled = true
      asg_min_size = 1
      asg_max_size = 3
      # pre_userdata = <<-EOT
      #     #!/bin/bash
      #     echo "Running custom init script"
      #     # Add your custom script commands here
      # EOT
      additional_security_group_ids = [aws_security_group.allow_access_from_dg.id]
    }
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
