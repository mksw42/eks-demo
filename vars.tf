# variable "access_key" {}
# variable "secret_key" {}
variable "region" {
  default = "eu-central-1"
}

variable "vault_token" {
  description = "Token pre prístup do HashiCorp Vault"
  sensitive = true
}
variable "runner_token" {
}

variable "agent_token" {
}
