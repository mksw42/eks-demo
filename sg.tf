resource "aws_security_group" "allow_access_from_dg" {
    name        = "allow_access_from_dg"
    description = "Security group to allow access only from specific IPs"
    vpc_id = module.vpc.vpc_id

    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"] #Add your IP from VPN
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
