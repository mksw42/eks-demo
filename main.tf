provider "aws" {
  region     = var.region
  access_key = data.vault_generic_secret.aws_creds.data["access_key"]
  secret_key = data.vault_generic_secret.aws_creds.data["secret_key"]
}

provider "vault" {
  address = "https://vault.test.e-d.sk/"
  token   = var.vault_token
}

data "vault_generic_secret" "aws_creds" {
  path = "SLX/Digmia"
}
