provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

resource "kubernetes_namespace" "gitlab_runner" {
  metadata {
    name = "gitlab-runner"
  }
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    token                  = data.aws_eks_cluster_auth.cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }
}

resource "helm_release" "gitlab_runner" {
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  namespace  = kubernetes_namespace.gitlab_runner.metadata[0].name

  set {
    name  = "gitlabUrl"
    value = "https://gitlab.essential-data.sk/"
  }

  set {
    name  = "runnerRegistrationToken"
    value = var.runner_token
  }

  depends_on = [
    kubernetes_namespace.gitlab_runner
  ]
}

resource "kubernetes_namespace" "gitlab_agent" {
  metadata {
    name = "gitlab-agent-telekom-dev"
  }
}

resource "helm_release" "gitlab_agent" {
  name       = "telekom-dev"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-agent"
  namespace  = kubernetes_namespace.gitlab_agent.metadata[0].name

  set {
    name  = "image.tag"
    value = "v16.2.0"
  }

  set {
    name  = "config.token"
    value = var.agent_token
  }

  set {
    name  = "config.kasAddress"
    value = "wss://gitlab.essential-data.sk/-/kubernetes-agent/"
  }

  depends_on = [
    kubernetes_namespace.gitlab_agent
  ]
}
