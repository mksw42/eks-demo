terraform {
  backend "s3" {
    bucket = "digmia-tf-state-bucket"
    key    = "Env/dev-tf-statefile"
    region = "eu-central-1"
    dynamodb_table = "insert_tablefor_lock"
    encrypt        = true
  }
}
